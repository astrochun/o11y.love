# SLO (Service Level Objective)

## Overview 

- [Specifications: OpenSLO](../collections-specs#openslo)
- [Platforms: Nobl9](../platforms#nobl9)

## SLO Management

### Pyrra

> Making SLOs with Prometheus manageable, accessible, and easy to use for everyone! 

- [Website](https://github.com/pyrra-dev/pyrra)

#### Facts

- Matthias Loibl's SLO calculator became a community OSS project in 2021.

#### Hot Topics 


### SLOth

> Easy and simple Prometheus SLO (service level objectives) generator

- [Website](https://github.com/slok/sloth)

#### Facts

- []()

#### Hot Topics 

### 

